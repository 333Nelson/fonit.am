<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteInformation extends Model
{
    protected $fillable = [
        'logo',
        'phone',
        'email',
        'business_hours',
        'address',
        'facebook',
        'instagram',
        'twitter',
        'telegram'
    ];
}
