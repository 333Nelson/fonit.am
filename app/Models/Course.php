<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'image',
        'title',
        'description',
        'course_category_id',
        'price',
        'is_deleted',
    ];

    public function getCourseCategory()
    {
        return $this->hasOne(CourseCategory::class, 'id', 'course_category_id');
    }

    public function getCourseFeatures()
    {
        return $this->hasOne(CourseFeature::class, 'course_id', 'id');
    }

    public function getCourseInstructors()
    {
        return $this->belongsToMany(User::class, 'course_instructors', 'course_id', 'user_id');
    }

    public function getCourseCurriculum()
    {
        return $this->hasMany(Curriculum::class);
    }
}
