<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    protected $fillable = [
        'name'
    ];

    public function getCourses()
    {
        return $this->hasMany(Course::class)->where('is_deleted', 0);
    }
}
