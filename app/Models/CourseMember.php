<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseMember extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'course_category_id',
        'token',
    ];

    public function getCourseCategory()
    {
        return $this->hasOne(CourseCategory::class, 'id', 'course_category_id');
    }
}
