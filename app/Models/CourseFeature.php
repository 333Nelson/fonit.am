<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseFeature extends Model
{
    protected $fillable = [
        'course_id',
        'duration',
        'lectures',
    ];
}
