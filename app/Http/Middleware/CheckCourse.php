<?php

namespace App\Http\Middleware;

use App\Models\Course;
use Closure;

class CheckCourse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $course = Course::find($request->course_id);

        if ($course->is_deleted === 1) {
            \abort(403);
        }

        return $next($request);
    }
}
