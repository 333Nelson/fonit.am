<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\CourseMember;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $users = User::where('role', "!=", 'admin')->orderBy('id', 'desc')->paginate(5);

        return view('dashboard.home', [
            'user' => $user,
            'users' => $users,
        ]);
    }

    public function registeredUsers()
    {
        $allRegisteredMembersCount = CourseMember::count();
        $courseMembers = CourseMember::with('getCourseCategory')->paginate(5);

        return view('dashboard.course.registered_users', [
            'allRegisteredMembersCount' => $allRegisteredMembersCount,
            'courseMembers' => $courseMembers,
        ]);
    }

    public function allCourses()
    {
        $courses = Course::with('getCourseCategory')->get();

        return view('dashboard.course.all_courses', [
            'courses' => $courses,
        ]);
    }

    public function deleteCourse($id)
    {
        $course = Course::find($id);
        $course->is_deleted = 1;
        $course->save();

        return back();
    }

    public function returnCourse($id)
    {
        $course = Course::find($id);
        $course->is_deleted = 0;
        $course->save();

        return back();
    }
}
