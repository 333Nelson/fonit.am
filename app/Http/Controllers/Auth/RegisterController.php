<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\CourseMember;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm($token)
    {
        $isExistsUser = CourseMember::where('token', $token)->exists();
        if (strlen($token) !== 32 || !$isExistsUser) \abort(404);

        $user = CourseMember::where('token', $token)->first();
        return view('auth.register', [
            'user' => $user,
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $token = $request->post('registrationToken');
        $isExistsUser = CourseMember::where('token', $token)->exists();

        if (!$isExistsUser) \abort(403);

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = CourseMember::where('token', $token)->first();
        $registeredUser = User::create([
            'role' => 'student',
            'name' => $user['name'],
            'email' => $user['email'],
            'phone' => $user['phone'],
            'password' => Hash::make($request->post('password')),
        ]);

        $this->guard()->login($registeredUser);
        CourseMember::destroy($user->id);
        return redirect('/home');
    }
}
