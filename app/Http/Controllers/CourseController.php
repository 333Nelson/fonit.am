<?php

namespace App\Http\Controllers;

use App\Mail\CourseRegistration;
use App\Mail\SendAdmin;
use App\Models\CourseMember;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class CourseController extends Controller
{
    public function register(Request $request)
    {
        $name = $request->post('name');
        $email = $request->post('email');
        $phone = $request->post('phone');
        $courseCategory = $request->post('courseCategory');

        $isValidateEmail = $this->validateEmail($email);
        $isValidatePhone = $this->validatePhone($phone);

        if ($isValidateEmail['validate'] && $isValidatePhone['validate']) {
            $token = Str::random(32);
            CourseMember::create([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'course_category_id' => $courseCategory,
                'token' => $token
            ]);

            $objDemo = new \stdClass();
            $objDemo->phone = $phone;
            $objDemo->email = $email;
            $objDemo->url = url('/register', $token);
            $objDemo->receiver = $name;

            Mail::to($email)->send(new CourseRegistration($objDemo));
            Mail::to("drycode2020@gmail.com")->send(new SendAdmin($objDemo));
        }

        return response()->json([
            'email' => $isValidateEmail,
            'phone' => $isValidatePhone,
        ]);
    }

    public static function validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return ['validate' => false, 'message' => 'Invalid E-mail'];
        }

        $isExistsEmail = CourseMember::where('email', $email)->exists();
        if ($isExistsEmail) {
            return ['validate' => false, 'message' => 'This E-mail already exists'];
        }

        return ['validate' => true, 'message' => 'success'];
    }

    public static function validatePhone($phone)
    {
        if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{3}$/", $phone)) {
            return ['validate' => false, 'message' => 'Invalid phone'];
        }

        $isExistsPhone = CourseMember::where('phone', $phone)->exists();
        if ($isExistsPhone) {
            return ['validate' => false, 'message' => 'This phone already exists'];
        }

        return ['validate' => true, 'message' => 'success'];
    }
}
