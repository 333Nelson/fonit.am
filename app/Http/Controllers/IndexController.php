<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseInstructor;
use App\Models\SiteInformation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class IndexController extends Controller
{
    public function welcome()
    {
        $courseCategories = CourseCategory::where('id', 1)->with('getCourses')->first();

        return view('welcome', [
            'courseCategories' => $courseCategories
        ]);
    }

    public function courses()
    {
        $courseCategory = CourseCategory::where('id', 1)->with('getCourses')->first();

        return view('pages.courses', [
            'courseCategory' => $courseCategory
        ]);
    }

    public function singleCourse($course_id)
    {
        $course = Course::where('id', $course_id)->with('getCourseCategory', 'getCourseFeatures', 'getCourseInstructors', 'getCourseCurriculum')->first();
        $allCourses = Course::where('id', '!=', $course_id)->get();

        return view('pages.single-course', [
            'course' => $course,
            'allCourses' => $allCourses,
        ]);
    }

    public function instructors()
    {
        $instructors = User::has('allInstructors')->get();

        return view('pages.instructors', [
            'instructors' => $instructors
        ]);
    }

    public function contact()
    {
        $siteInformation = SiteInformation::find(1);
        return view('pages.contact', [
            'site_information' => $siteInformation
        ]);
    }

    public function createContact(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email:rfc,dns',
            'message' => 'required',
        ]);

        $name = $request->post('name');
        $email = $request->post('email');
        $message = $request->post('message');

        ContactUs::create([
            'name' => $name,
            'email' => $email,
            'message' => $message,
        ]);

        return back();
    }

    public function lang($locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);

        return redirect()->back();
    }
}
