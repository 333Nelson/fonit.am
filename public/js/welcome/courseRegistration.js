$(function () {
    $('form#courseRegistration').on('submit', function (event) {
        event.preventDefault();
        let that = $(this);
        let button = that.find('button');
        let buttonText = button.text();
        button.attr('disabled', true);
        button.text('Loading...');
        let data = that.serialize();

        $.ajax({
            url: '/course/registration',
            method: 'post',
            data: data,
            dataType: 'json',
            success: function (res) {
                let errorsContainer = $('div.errors');
                errorsContainer.empty();
                if (res['email']['validate'] && res['phone']['validate']) {
                    that.find("input[type=text]").val("");
                    errorsContainer.append('<p class="text-success">Խնդրում ենք ստւգել ձեր E-mail - ը։</p>')
                } else {
                    if (!res['email']['validate']) {
                        errorsContainer.append(`<p class="text-danger">${res['email']['message']}</p>`);
                    }

                    if (!res['phone']['validate']) {
                        errorsContainer.append(`<p class="text-danger">${res['phone']['message']}</p>`);
                    }
                }

                button.attr('disabled', false);
                button.text(buttonText);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
});
