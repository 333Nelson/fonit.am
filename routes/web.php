<?php
use App\Models\SiteInformation;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

View::composer('layouts.app', function($view)
{
    $siteInformation = SiteInformation::first();
    $view->with(
        [
            'siteInformation' => $siteInformation,
        ]);
});

Route::get('/', 'IndexController@welcome');
Route::get('/lang/{locale}', 'IndexController@lang');
Route::get('/courses', 'IndexController@courses');
Route::get('/single-course/{course_id}', 'IndexController@singleCourse')->middleware('checkCourse');
Route::get('/instructors', 'IndexController@instructors');
Route::get('/contact', 'IndexController@contact');
Route::post('/course/registration', 'CourseController@register');
Route::post('/contact-us', 'IndexController@createContact')->name('contact-us');

Auth::routes();


// SuperAdmin
Route::group(['prefix' => "dashboard", 'middleware' => 'checkAdmin'], function () {
    Route::get('/', 'Dashboard\DashboardController@index');
    Route::get('/registered-users', 'Dashboard\DashboardController@registeredUsers');

    // Courses
    Route::group(['prefix' => "courses"], function () {
        Route::get('/', 'Dashboard\DashboardController@allCourses');
        Route::get('/delete/{id}', 'Dashboard\DashboardController@deleteCourse');
        Route::get('/return/{id}', 'Dashboard\DashboardController@returnCourse');
//        Route::post('/add', 'Dashboard\DashboardController@addNews')->name('add-news');
//        Route::post('/edit', 'Dashboard\DashboardController@editNews')->name('edit-news');
//        Route::post('/delete', 'Dashboard\DashboardController@deleteNews')->name('delete-news');
//        Route::post('/change-news-status', 'Dashboard\DashboardController@editNewsStatus')->name('change-news-status');
    });
});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/register', 'Auth\RegisterController@create')->name('register');
Route::get('/register/{token}', 'Auth\RegisterController@showRegistrationForm');
