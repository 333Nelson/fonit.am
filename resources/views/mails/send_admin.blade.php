Բարև {{ $sender }},
<p>Հաջողությամբ գրանցվեց նոր աշակերտ․</p>

<p>Անուն - {{ $obj->receiver }}</p>
<p>Էլ․ փոստ - {{ $obj->email }}</p>
<p>Հեռ․ - {{ $obj->phone }}</p>

<br/>

Շնորհակալություն,
<br/>
<strong>{{ $sender }}</strong>
