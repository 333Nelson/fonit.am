<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('storage/img/core-img/favicon.png') }}">
</head>
<body>
    <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
              FB.init({
                xfbml            : true,
                version          : 'v8.0'
              });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
        attribution=setup_tool
        page_id="102760984912143"
        theme_color="#3762f0">
    </div>

    <div id="app">
        <!-- Preloader -->
        <div id="preloader">
            <div class="spinner"></div>
        </div>

        <!-- ##### Header Area Start ##### -->
        <header class="header-area">
            <!-- Top Header Area -->
            <div class="top-header-area d-flex justify-content-between align-items-center">
                <!-- Contact Info -->
                <div class="contact-info">
                    <a href="tel:{{ $siteInformation['phone'] }}">
                        <span>
                            {{ trans('form.phone', [], \Session::get('locale')) }}:
                        </span> {{ $siteInformation['phone'] }}
                    </a>
                    <a href="mailto:{{ $siteInformation['email'] }}">
                        <span>{{ trans('form.email', [], \Session::get('locale')) }}:</span> {{ $siteInformation['email'] }}
                    </a>
                </div>
                <!-- Follow Us -->
                <div class="follow-us">
                    <span>{{ trans('form.followUs', [], \Session::get('locale')) }}</span>
                    <a href="{{ $siteInformation['facebook'] }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="{{ $siteInformation['instagram'] }}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="{{ $siteInformation['twitter'] }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
{{--                    <a href="{{ $siteInformation['telegram'] }}"><i class="fa fa-telegram" aria-hidden="true"></i></a>--}}
                </div>
            </div>

            <!-- Navbar Area -->
            <div class="clever-main-menu">
                <div class="classy-nav-container breakpoint-off">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="cleverNav">

                        <!-- Logo -->
                        <a class="nav-brand" href="{{ url('/') }}">
                            <img src="{{ asset('storage/img/core-img/' . $siteInformation['logo']) }}" alt="Logo">
                        </a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">
                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap">
                                    <span class="top"></span>
                                    <span class="bottom"></span>
                                </div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{ url('/courses') }}">{{ trans('menu.courses', [], \Session::get('locale')) }}</a></li>
                                    <li><a href="{{ url('/instructors') }}">{{ trans('menu.instructors', [], \Session::get('locale')) }}</a></li>
                                    <li><a href="{{ url('/contact') }}">{{ trans('menu.contact', [], \Session::get('locale')) }}</a></li>
                                    <li><a href="#">{{ trans('menu.lang', [], \Session::get('locale')) }}</a>
                                        <ul class="dropdown">
                                            <li>
                                                <a href="{{ url('/lang/am') }}">
                                                    <img src="{{ asset('storage/img/bg-img/am.gif') }}" alt="AM" class="w-25">
                                                    hay
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/lang/ru') }}">
                                                    <img src="{{ asset('storage/img/bg-img/ru.gif') }}" alt="RU" class="w-25">
                                                    rus
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/lang/en') }}">
                                                    <img src="{{ asset('storage/img/bg-img/en.gif') }}" alt="EN" class="w-25">
                                                    eng
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                                <!-- Search Button -->
                                <div class="search-area">
                                    <form action="#" method="post">
{{--                                        <input type="search" name="search" id="search" placeholder="Search">--}}
{{--                                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>--}}
                                    </form>
                                </div>

                                <!-- Register / Login -->
                                @if (Route::has('login'))
                                    <div class="register-login-area">
                                        @auth
                                            <a href="{{ url('/home') }}" class="btn active">{{ \Auth()->user()->name }}</a>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();" class="btn">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        @else
                                            <a href="{{ route('login') }}" class="btn active">{{ trans('menu.login', [], \Session::get('locale')) }}</a>
                                        @endauth
                                    </div>
                                @endif
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!-- ##### Header Area End ##### -->

        <main style="min-height: 55vh;">
            @yield('content')
        </main>

        <!-- ##### Footer Area Start ##### -->
        <footer class="footer-area">
            <!-- Top Footer Area -->
            <div class="top-footer-area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!-- Footer Logo -->
                            <div class="footer-logo">
                                <a href="{{ url('/') }}">DryCode</a>
                            </div>
                            <!-- Copywrite -->
                            <p>
                                <a href="#">
                                    Copyright &copy; {{ date('Y') }}
                                    {{ trans('menu.copyRight', [], \Session::get('locale')) }}
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Bottom Footer Area -->
            <div class="bottom-footer-area d-flex justify-content-between align-items-center">
                <!-- Contact Info -->
                <div class="contact-info">
                    <a href="tel:{{ $siteInformation['phone'] }}">
                        <span>{{ trans('form.phone', [], \Session::get('locale')) }}:</span>
                        {{ $siteInformation['phone'] }}
                    </a>
                    <a href="mailto:{{ $siteInformation['email'] }}">
                        <span>{{ trans('form.email', [], \Session::get('locale')) }}:</span>
                        {{ $siteInformation['email'] }}
                    </a>
                </div>
                <!-- Follow Us -->
                <div class="follow-us">
                    <span>{{ trans('form.followUs', [], \Session::get('locale')) }}</span>
                    <a href="{{ $siteInformation['facebook'] }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="{{ $siteInformation['instagram'] }}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="{{ $siteInformation['twitter'] }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
{{--                    <a href="{{ $siteInformation['telegram'] }}"><i class="fa fa-telegram" aria-hidden="true"></i></a>--}}
                </div>
            </div>
        </footer>
        <!-- ##### Footer Area End ##### -->
    </div>
    <!-- jQuery-2.2.4 js -->
    <script src="{{ asset('js/jquery/jquery-2.2.4.min.js') }}"></script>
    <!-- Popper js -->
    <script src="{{ asset('js/bootstrap/popper.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <!-- All Plugins js -->
    <script src="{{ asset('js/plugins/plugins.js') }}"></script>
    <!-- Active js -->
    <script src="{{ asset('js/active.js') }}"></script>

    <!-- Scripts -->
    @stack('scripts')
</body>
</html>
