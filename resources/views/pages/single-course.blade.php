@extends('layouts.app')
@section('title', 'DryCode | Single Course')

@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('/') }}">
                        {{ trans('breadcrumb.home', [], \Session::get('locale')) }}
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ url('/courses') }}">
                        {{ trans('breadcrumb.courses', [], \Session::get('locale')) }}
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{ $course['getCourseCategory']['name'] }}</li>
                <li class="breadcrumb-item active" aria-current="page">{{ $course['title'] }}</li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Single Course Intro Start ##### -->
    <div class="single-course-intro d-flex align-items-center justify-content-center" style="background-image: url('{{ asset('storage/img/bg-img/bg3.jpg') }}');">
        <!-- Content -->
        <div class="single-course-intro-content text-center">
            <h3>{{ $course['title'] }}</h3>
            <div class="meta d-flex align-items-center justify-content-center">
                <a href="#">{{ $course['getCourseCategory']['name'] }}</a>
            </div>
            <div class="price">{{ $course['price'] }}</div>
        </div>
    </div>
    <!-- ##### Single Course Intro End ##### -->

    <!-- ##### Courses Content Start ##### -->
    <div class="single-course-content section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="course--content">

                        <div class="clever-tabs-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="true">
                                        {{ trans('singleCourse.description', [], \Session::get('locale')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab--2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">
                                        {{ trans('singleCourse.curriculum', [], \Session::get('locale')) }}
                                    </a>
                                </li>
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" id="tab--3" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true">Reviews</a>--}}
{{--                                </li>--}}
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <!-- Tab Text -->
                                <div class="tab-pane fade" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                                    <div class="clever-description">

                                        <!-- About Course -->
                                        <div class="about-course mb-30">
                                            <h4>{{ trans('singleCourse.about', [], \Session::get('locale')) }}</h4>
                                            <p>{{ $course['description'] }}</p>
                                        </div>

                                        <!-- All Instructors -->
                                        <div class="all-instructors mb-30">
                                            <h4>{{ trans('singleCourse.thisCourseInstructors', [], \Session::get('locale')) }}</h4>

                                            <div class="row">
                                                <!-- Single Instructor -->
                                                @forelse($course['getCourseInstructors'] as $instructor)
                                                    <div class="col-lg-6">
                                                        <div class="single-instructor d-flex align-items-center mb-30">
                                                            <div class="instructor-thumb">
                                                                <img src="{{ asset('storage/img/bg-img/t2.png') }}" alt="{{ $instructor['role'] }}">
                                                            </div>
                                                            <div class="instructor-info">
                                                                <h5>{{ $instructor['name'] }}</h5>
                                                                <span>{{ $instructor['role'] }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    Empty!!
                                                @endforelse
                                            </div>
                                        </div>

                                        <!-- FAQ -->
                                        <div class="clever-faqs">
                                            <h4>FAQs</h4>

                                            <div class="accordions" id="accordion" role="tablist" aria-multiselectable="true">

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6><a role="button" class="" aria-expanded="true" aria-controls="collapseOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Can I just enroll in a single course? I'm not interested in the entire Specialization?
                                                            <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        </a></h6>
                                                    <div id="collapseOne" class="accordion-content collapse show">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis fringilla tortor.</p>
                                                    </div>
                                                </div>

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" class="collapsed" aria-expanded="true" aria-controls="collapseTwo" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo">What is the refund policy?
                                                            <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseTwo" class="accordion-content collapse">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel lectus eu felis semper finibus ac eget ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vulputate id justo quis facilisis.</p>
                                                    </div>
                                                </div>

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" aria-expanded="true" aria-controls="collapseThree" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseThree">What background knowledge is necessary?
                                                            <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseThree" class="accordion-content collapse">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel lectus eu felis semper finibus ac eget ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vulputate id justo quis facilisis.</p>
                                                    </div>
                                                </div>

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" aria-expanded="true" aria-controls="collapseFour" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseFour">Do i need to take the courses in a specific order?
                                                            <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseFour" class="accordion-content collapse">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel lectus eu felis semper finibus ac eget ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vulputate id justo quis facilisis.</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- Tab Text -->
                                <div class="tab-pane fade show active" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                                    <div class="clever-curriculum">
                                        <!-- Curriculum Level -->
                                        <div class="curriculum-level mb-30">
                                            <ul class="curriculum-list">
                                                <li>
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    <strong>Յուրաքանչյուր դաս 1,5 - 2 ժամ տեվողությամբ ՝</strong>
                                                    <ul>
                                                        @forelse($course['getCourseCurriculum'] as $key => $item)
                                                            <li>
                                                                <span>
                                                                    <strong>{{ $key+1 }})</strong>
                                                                    <span>{{ $item['title'] }}</span>
                                                                </span>
                                                            </li>
                                                        @empty
                                                            {{ trans('form.noResults', [], \Session::get('locale')) }}
                                                        @endforelse
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <!-- Tab Text -->
                                {{--<div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab--3">
                                    <div class="clever-review">

                                        <!-- About Review -->
                                        <div class="about-review mb-30">
                                            <h4>Reviews</h4>
                                            <p>Sed elementum lacus a risus luctus suscipit. Aenean sollicitudin sapien neque, in fermentum lorem dignissim a. Nullam eu mattis quam. Donec porttitor nunc a diam molestie blandit. Maecenas quis ultrices</p>
                                        </div>

                                        <!-- Ratings -->
                                        <div class="clever-ratings d-flex align-items-center mb-30">

                                            <div class="total-ratings text-center d-flex align-items-center justify-content-center">
                                                <div class="ratings-text">
                                                    <h2>4.5</h2>
                                                    <div class="ratings--">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                                    </div>
                                                    <span>Rated 5 out of 3 Ratings</span>
                                                </div>
                                            </div>

                                            <div class="rating-viewer">
                                                <!-- Rating -->
                                                <div class="single-rating mb-15 d-flex align-items-center">
                                                    <span>5 STARS</span>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span>80%</span>
                                                </div>
                                                <!-- Rating -->
                                                <div class="single-rating mb-15 d-flex align-items-center">
                                                    <span>4 STARS</span>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span>20%</span>
                                                </div>
                                                <!-- Rating -->
                                                <div class="single-rating mb-15 d-flex align-items-center">
                                                    <span>3 STARS</span>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span>0%</span>
                                                </div>
                                                <!-- Rating -->
                                                <div class="single-rating mb-15 d-flex align-items-center">
                                                    <span>2 STARS</span>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span>0%</span>
                                                </div>
                                                <!-- Rating -->
                                                <div class="single-rating mb-15 d-flex align-items-center">
                                                    <span>1 STARS</span>
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span>0%</span>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Single Review -->
                                        <div class="single-review mb-30">
                                            <div class="d-flex justify-content-between mb-30">
                                                <!-- Review Admin -->
                                                <div class="review-admin d-flex">
                                                    <div class="thumb">
                                                        <img src="{{ asset('storage/img/bg-img/t1.png') }}" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h6>Sarah Parker</h6>
                                                        <span>Sep 29, 2017 at 9:48 am</span>
                                                    </div>
                                                </div>
                                                <!-- Ratings -->
                                                <div class="ratings">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis.</p>
                                        </div>

                                        <!-- Single Review -->
                                        <div class="single-review mb-30">
                                            <div class="d-flex justify-content-between mb-30">
                                                <!-- Review Admin -->
                                                <div class="review-admin d-flex">
                                                    <div class="thumb">
                                                        <img src="{{ asset('storage/img/bg-img/t1.png') }}" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h6>Sarah Parker</h6>
                                                        <span>Sep 29, 2017 at 9:48 am</span>
                                                    </div>
                                                </div>
                                                <!-- Ratings -->
                                                <div class="ratings">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis.</p>
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="course-sidebar">
                        <!-- Buy Course -->
                        <a href="#" class="btn clever-btn mb-30 w-100">
                            {{ trans('singleCourse.buyCourse', [], \Session::get('locale')) }}
                        </a>

                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h4>{{ trans('singleCourse.courseFeatures', [], \Session::get('locale')) }}</h4>
                            <ul class="features-list">
                                <li>
                                    <h6>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        {{ trans('singleCourse.duration', [], \Session::get('locale')) }}
                                    </h6>
                                    <h6>
                                        {{ $course['getCourseFeatures']['duration'] }}
                                        {{ trans('singleCourse.hour', [], \Session::get('locale')) }}
                                    </h6>
                                </li>
                                <li>
                                    <h6>
                                        <i class="fa fa-bell" aria-hidden="true"></i>
                                        {{ trans('singleCourse.lectures', [], \Session::get('locale')) }}
                                    </h6>
                                    <h6>{{ $course['getCourseFeatures']['lectures'] }}</h6>
                                </li>
                                {{--<li>
                                    <h6><i class="fa fa-file" aria-hidden="true"></i> Quizzes</h6>
                                    <h6>3</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-up" aria-hidden="true"></i> Pass Percentage</h6>
                                    <h6>60</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-down" aria-hidden="true"></i> Max Retakes</h6>
                                    <h6>5</h6>
                                </li>--}}
                            </ul>
                        </div>

                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h4>{{ trans('singleCourse.certification', [], \Session::get('locale')) }}</h4>
                            <img src="{{ asset('storage/img/bg-img/cer.png') }}" alt="Certification">
                        </div>

                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h4>{{ trans('singleCourse.youMayLike', [], \Session::get('locale')) }}</h4>

                            <!-- Single Courses -->
                            @forelse($allCourses as $singleCourse)
                                <div class="single--courses d-flex align-items-center">
                                    <div class="thumb">
                                        <img src="{{ asset('storage/img/bg-img/' . $singleCourse['image']) }}" alt="">
                                    </div>
                                    <div class="content">
                                        <a href="{{ url('/single-course/' . $singleCourse['id']) }}">{{ $singleCourse['title'] }}</a>
                                        <h6>{{ $singleCourse['price'] }}</h6>
                                    </div>
                                </div>
                            @empty
                                {{ trans('form.noResults', [], \Session::get('locale')) }}
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Courses Content End ##### -->
@endsection
