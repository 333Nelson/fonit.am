@extends('layouts.app')
@section('title', 'DryCode | Contact')

@section('content')
    <style>
        #map {
            width: 100%;
            height: 400px;
        }
    </style>
    <script src="{{ url('https://maps.googleapis.com/maps/api/js?key=AIzaSyAJYR4ScDHLCle9N9ni43PmRW4hQ-OC22w&callback=initMap') }}" defer></script>
    <script type="text/javascript">
        "use strict";

        let map;

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                center: {
                    lat: 40.177,
                    lng: 44.503
                },
                zoom: 8
            });
        }
    </script>
    <!-- ##### Google Maps ##### -->

    <div id="map"></div>

    <!-- ##### Contact Area Start ##### -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                <!-- Contact Info -->
                <div class="col-12 col-lg-6">
                    <div class="contact--info mt-50 mb-100">
                        <h4>{{ trans('form.contactInfo', [], \Session::get('locale')) }}</h4>
                        <ul class="contact-list">
                            <li>
                                <h6>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    {{ trans('form.businessHours', [], \Session::get('locale')) }}
                                </h6>
                                <h6>{{ $site_information['business_hours'] }}</h6>
                            </li>
                            <li>
                                <h6>
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    {{ trans('form.phone', [], \Session::get('locale')) }}
                                </h6>
                                <h6>{{ $site_information['phone'] }}</h6>
                            </li>
                            <li>
                                <h6>
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    {{ trans('form.email', [], \Session::get('locale')) }}
                                </h6>
                                <h6>{{ $site_information['email'] }}</h6>
                            </li>
                            <li>
                                <h6>
                                    <i class="fa fa-map-pin" aria-hidden="true"></i>
                                    {{ trans('form.address', [], \Session::get('locale')) }}
                                </h6>
                                <h6>{{ $site_information['address'] }}</h6>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Contact Form -->
                <div class="col-12 col-lg-6">
                    <div class="contact-form">
                        <h4>{{ trans('form.getInTouch', [], \Session::get('locale')) }}</h4>

                        <form action="{{ route('contact-us') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input
                                            name="name"
                                            type="text"
                                            class="form-control"
                                            id="text"
                                            placeholder="{{ trans('form.name', [], \Session::get('locale')) }}"
                                        />
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input
                                            name="email"
                                            type="email"
                                            class="form-control"
                                            id="email"
                                            placeholder="{{ trans('form.email', [], \Session::get('locale')) }}"
                                        />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea
                                            name="message"
                                            class="form-control"
                                            id="message"
                                            cols="30"
                                            rows="10"
                                            placeholder="{{ trans('form.message', [], \Session::get('locale')) }}"
                                        ></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    @if($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $message)
                                                    <li>{{ $message }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-12">
                                    <button class="btn clever-btn w-100">
                                        {{ trans('form.sendMessage', [], \Session::get('locale')) }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->

@endsection

