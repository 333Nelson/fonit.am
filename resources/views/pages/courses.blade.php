@extends('layouts.app')
@section('title', 'DryCode | Courses')

@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('/') }}">
                        {{ trans('breadcrumb.home', [], \Session::get('locale')) }}
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{ trans('breadcrumb.courses', [], \Session::get('locale')) }}
                </li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Catagory ##### -->
    <div class="clever-catagory bg-img d-flex align-items-center justify-content-center p-3" style="background-image: url('{{ asset('storage/img/bg-img/bg2.jpg') }}');">
        <h3>{{ $courseCategory['name'] }}</h3>
    </div>

    <!-- ##### Popular Course Area Start ##### -->
    <section class="popular-courses-area section-padding-100">
        <div class="container">
            <div class="row">
                <!-- Single Popular Course -->
                @forelse($courseCategory['getCourses'] as $course)
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                            <img src="{{ asset('storage/img/bg-img/' . $course['image']) }}" alt="" style="height: 235px;">
                            <!-- Course Content -->
                            <div class="course-content">
                                <h4>{{ $course['title'] }}</h4>
                                <div class="meta d-flex align-items-center">
                                    <a href="#">{{ $courseCategory['name'] }}</a>
                                </div>
                            </div>
                            <!-- Seat Rating Fee -->
                            <div class="seat-rating-fee d-flex justify-content-between">
                                <div class="seat-rating h-100 d-flex align-items-center">
                                    {{--<div class="seat">
                                        <i class="fa fa-user" aria-hidden="true"></i> 10
                                    </div>
                                    <div class="rating">
                                        <i class="fa fa-star" aria-hidden="true"></i> 4.5
                                    </div>--}}
                                </div>
                                <div class="course-fee h-100">
                                    <a href="{{ url('/single-course/' . $course['id']) }}" class="free">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <h1>Empty!!!</h1>
                @endforelse
            </div>
        </div>
    </section>
    <!-- ##### Popular Course Area End ##### -->
@endsection
