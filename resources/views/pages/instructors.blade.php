@extends('layouts.app')
@section('title', 'DryCode | Instructors')

@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('/') }}">
                        {{ trans('breadcrumb.home', [], \Session::get('locale')) }}
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{ trans('breadcrumb.instructors', [], \Session::get('locale')) }}
                </li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Instructors Video Start ##### -->
    <div class="instructors-video d-flex align-items-center justify-content-center bg-img" style="background-image: url('{{ asset('storage/img/bg-img/bg4.jpg') }}');">
        <h2>Be The Best Teacher</h2>
        <!-- video btn -->
        <a href="https://www.youtube.com/watch?v=qC_T9ePzANg" class="video-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
    </div>
    <!-- ##### Instructors Video End ##### -->

    <!-- ##### Best Tutors Area Start ##### -->
    {{--<section class="best-tutors-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>The Best Tutors in Town</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="tutors-slide owl-carousel">
                        <!-- Single Tutors Slide -->
                        @forelse($instructors as $instructor)
                            <div class="single-tutors-slides">
                                <!-- Tutor Thumbnail -->
                                <div class="tutor-thumbnail">
                                    <img src="{{ asset('storage/img/bg-img/t2.png') }}" alt="Instructor">
                                </div>
                                <!-- Tutor Information -->
                                <div class="tutor-information text-center">
                                    <h5>{{ $instructor['name'] }}</h5>
                                    <span>{{ $instructor['role'] }}</span>

                                    <div class="social-info">
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            Empty!!
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    <!-- ##### Best Tutors Area End ##### -->

    <!-- ##### Top Teacher Area Start ##### -->
    <section class="top-teacher-area section-padding-0-100 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Top Teachers in Every Field</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Teacher -->
                @forelse($instructors as $instructor)
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-instructor d-flex align-items-center mb-30">
                            <div class="instructor-thumb">
                                <img src="{{ asset('storage/img/bg-img/t2.png') }}" alt="Instructor">
                            </div>
                            <div class="instructor-info">
                                <h5>{{ $instructor['name'] }}</h5>
                                <span>{{ $instructor['role'] }}</span>
                            </div>
                        </div>
                    </div>
                @empty
                    Empty!!
                @endforelse
            </div>
        </div>
    </section>
    <!-- ##### Top Teacher Area End ##### -->
@endsection
