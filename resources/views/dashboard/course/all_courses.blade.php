@extends('layouts.appDashboard')
@section('title', 'Courses')
@section('content')
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Cards</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}" class="text-muted">Home</a></li>
                                <li class="breadcrumb-item text-muted active" aria-current="page">Courses</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-5 align-self-center">
                    <div class="customize-input float-right">
                        <select class="custom-select custom-select-set form-control bg-white border-0 custom-shadow custom-radius">
                            <option selected>Aug 19</option>
                            <option value="1">July 19</option>
                            <option value="2">Jun 19</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Row -->
                    <div class="row">
                        @forelse($courses as $course)
                            <!-- column -->
                                <div class="col-lg-4 col-md-6">
                                    <!-- Card -->
                                    <div class="card">
                                        <img class="card-img-top img-fluid" src="{{ asset('storage/img/bg-img/' . $course->image) }}"
                                             alt="{{ $course->title }}" style="height: 150px;">
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                {{ $course->title }}
                                                <span>({{ $course['getCourseCategory']['name'] }})</span>
                                            </h4>
                                            <p class="card-text">{{ $course->description }}</p>
                                            <a href="javascript:void(0)" class="btn btn-success">
                                                <i class="icon-note"></i>
                                                Edit
                                            </a>
                                            @if($course->is_deleted === 0)
                                                <a href="{{ url('/dashboard/courses/delete/' . $course->id) }}" class="btn btn-danger">
                                                    <i class="icon-ban"></i>
                                                    Delete
                                                </a>
                                            @else
                                                <a href="{{ url('/dashboard/courses/return/' . $course->id) }}" class="btn btn-orange">
                                                    <i class="icon-refresh"></i>
                                                    Return
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- Card -->
                                </div>
                            <!-- column -->
                        @empty
                            Empty!!
                        @endforelse
                    </div>
                    <!-- Row -->
                </div>
            </div>
            <!-- End Row -->
        </div>
    </div>
@endsection
