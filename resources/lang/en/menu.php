<?php

return [
    'courses' => 'Courses',
    'contact' => 'Contact',
    'instructors' => 'Instructors',
    'lang' => 'English',
    'register' => 'Register',
    'login' => 'Login',
    'copyRight' => 'All rights reserved'
];
