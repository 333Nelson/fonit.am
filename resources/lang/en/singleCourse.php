<?php

return [
    'youMayLike' => 'You May Like',
    'certification' => 'Certification',
    'thisCourseInstructors' => 'This Course Instructors',
    'about' => 'About this course',
    'curriculum' => 'Curriculum',
    'description' => 'Description',
    'courseFeatures' => 'Course Features',
    'buyCourse' => 'Buy Course',
    'hour' => 'Hour',
    'duration' => 'Duration',
    'lectures' => 'Lectures',
];
