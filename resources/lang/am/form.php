<?php

return [
    'email' => 'Էլեկտրոնային փոստ',
    'name' => 'Անուն',
    'phone' => 'Հեռախոսահամար',
    'password' => 'Գաղտնաբառ',
    'confirmPassword' => 'Հաստատել գաղտնաբառը',
    'oldPassword' => 'Հին Գաղտնաբառ',
    'newPassword' => 'Նոր Գաղտնաբառ',
    'username' => 'Օգտագործողի անունը',
    'lastname' => 'Ազգանուն',
    'firstname' => 'Անուն',
    'comments' => 'Մեկնաբանություններ',
    'pressEnter' => 'Մեկնաբանություն ստեղծելու համար սեղմեք Enter',
    'readMore' => 'Կարդալ ավելին',
    'back' => 'Ետ',
    'contactUs' => 'Կապ մեզ հետ',
    'allRightsReserved' => 'Բոլոր իրավունքները պաշտպանված են.',
    'subject' => 'Առարկա',
    'message' => 'Հաղորդագրություն',
    'submit' => 'Ուղարկել',
    'sendMessage' => 'Ուղարկել հաղորդագրություն',
    'successfullyEdited' => 'Հաջողությամբ խմբագրվեց',
    'requestQuote' => 'Հայցում մեջբերում',
    'forgotPassword' => 'Մոռացել եք Ձեր գաղտնաբառը?',
    'resetPassword' => 'Վերականգնել գաղտնաբառը',
    'eMailAddress' => 'Էլեկտրոնային հասցե',
    'sendPasswordResetLink' => 'Ուղարկեք գաղտնաբառի վերափոխման հղումը',
    'dashboard' => 'Ղեկավարման վահանակ',
    'noResults' => 'Արդյունքներ չկան',
    'search' => 'Որոնել',
    'accept' => "Ընդունել",
    'chatChooseUserMessage' => 'Խնդրում ենք ընտրել անձին',
    'createAnAccount' => 'Դեռ հաշիվ չունե՞ք:',
    'followUs' => 'Հետեւեք մեզ',
    'courseForWEB' => 'Դասընթացներ WEB- ի համար',
    'registerNow' => 'Գրանցվիր հիմա',
    'businessHours' => 'Բիզնես ժամեր',
    'contactInfo' => 'Կոնտակտային տվյալները',
    'address' => 'Հասցե',
    'getInTouch' => 'Կապվեք մեզ հետ',
];
