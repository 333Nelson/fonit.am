<?php

return [
    'youMayLike' => 'Դուք կարող եք հավանել',
    'certification' => 'Վկայական',
    'thisCourseInstructors' => 'Այս դասընթացի դասախոսները',
    'about' => 'Այս դասընթացի մասին',
    'curriculum' => 'Ուսումնական պլան',
    'description' => 'Նկարագրություն',
    'courseFeatures' => 'Դասընթացի առանձնահատկությունները',
    'buyCourse' => 'Գնիր դասընթաց',
    'hour' => 'Ժամ',
    'duration' => 'Տեւողությունը',
    'lectures' => 'Դասախոսություններ',
];
