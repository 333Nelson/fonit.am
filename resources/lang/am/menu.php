<?php

return [
    'courses' => 'Կուրսեր',
    'contact' => 'Հետադարձ կապ',
    'instructors' => 'Ուսուցիչներ',
    'lang' => 'Հայերեն',
    'register' => 'Գրանցվել',
    'login' => 'Մուտք',
    'copyRight' => 'Բոլոր իրավունքները պաշտպանված են'
];
