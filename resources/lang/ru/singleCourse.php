<?php

return [
    'youMayLike' => 'Вам может понравиться',
    'certification' => 'Сертификат',
    'thisCourseInstructors' => 'Инструкторы этого курса',
    'about' => 'Об этом курсе',
    'curriculum' => 'Учебный план',
    'description' => 'Описание',
    'courseFeatures' => 'Особенности курса',
    'buyCourse' => 'Купить курс',
    'hour' => 'Час',
    'duration' => 'Длительность',
    'lectures' => 'Лекции',
];
