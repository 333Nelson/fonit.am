<?php

return [
    'courses' => 'Курсы',
    'contact' => 'Контакты',
    'instructors' => 'Инструкторы',
    'lang' => 'Русский',
    'register' => 'Регистрация',
    'login' => 'Вход',
    'copyRight' => 'Все права защищены'
];
