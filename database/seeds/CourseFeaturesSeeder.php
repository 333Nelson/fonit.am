<?php

use App\Models\CourseFeature;
use Illuminate\Database\Seeder;

class CourseFeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseFeature::create([
            'course_id' => 1,
            'duration' => '1.5 - 2',
            'lectures' => 12,
        ]);

        CourseFeature::create([
            'course_id' => 2,
            'duration' => '1.5 - 2',
            'lectures' => 12,
        ]);

        CourseFeature::create([
            'course_id' => 3,
            'duration' => '1.5 - 2',
            'lectures' => 12,
        ]);

        CourseFeature::create([
            'course_id' => 4,
            'duration' => '1.5 - 2',
            'lectures' => 12,
        ]);

        CourseFeature::create([
            'course_id' => 5,
            'duration' => '1.5 - 2',
            'lectures' => 12,
        ]);
    }
}
