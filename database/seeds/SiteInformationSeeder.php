<?php

use App\Models\SiteInformation;
use Illuminate\Database\Seeder;

class SiteInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteInformation::create([
            'logo' => 'logo.png',
            'phone' => '097188001',
            'email' => 'drycode2020@gmail.com',
            'business_hours' => '10:00 AM - 20:00 PM',
            'address' => 'Armenia, Abovyan',
            'facebook' => 'https://www.facebook.com/drycode',
            'instagram' => 'https://www.instagram.com/_drycode_',
            'twitter' => 'https://twitter.com/dry_code',
            'telegram' => '#',
        ]);
    }
}
