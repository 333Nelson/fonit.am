<?php

use App\Models\Curriculum;
use Illuminate\Database\Seeder;

class CurriculumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // HTML & CSS
        Curriculum::create([
            'course_id' => 1,
            'title' => "Ի՞նչ է HTML -ը , ի՞նչ է թեգը , թեգերի տեսակները , ի՞նչ է ատրիբուտը , HTML էջի կառուցվածքը:",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "Հատուկ նշաններ , աղյուսակներ , ֆորմաներ։",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "Կայքի հիմնական կառուցվածքը , գրաֆիկ խմբագրիչներ , Adobe Photoshop։",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "ի՞նչ է CSS -ը, CSS-ի հայտարարման եղանակները , սելեկտորներ:",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "CSS-ի ժառանգություն , հատկությունների խմբագրում։",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "Overflow , position , հատկություններ, Psd ֆայլի նշագրում։",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "Ինչի համար են գրաֆիկական խմբագրիչները , Adobe Photoshop գրաֆիկական խմբագրիչ , Շերտեր։",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "CSS մեդիա։",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "Ի՞նչ է Grid–ը և ինչի համար է այն , Ինչպես օգտագործել Grid-ը:",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "Ի՞նչ է Flexbox–ը և ինչի համար է այն , Ինչպես օգտագործել Flexbox-ը:",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "Ի՞նչ է BootStrap–ը և ինչի համար է այն , Ինչպես տեղադրել BootStrap 4-ը:",
        ]);

        Curriculum::create([
            'course_id' => 1,
            'title' => "CSS3-ի հատկություններ, անիմացիա։",
        ]);
    }
}
