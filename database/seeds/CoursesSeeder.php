<?php

use App\Models\Course;
use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::create([
            'image' => 'c1.jpg',
            'title' => 'HTML & CSS',
            'course_category_id' => 1,
            'price' => '33.000դր'
        ]);

        Course::create([
            'image' => 'c2.jpg',
            'title' => 'Sass & less',
            'course_category_id' => 1,
            'price' => '33.000դր'
        ]);

        Course::create([
            'image' => 'c3.jpg',
            'title' => 'JavaScript & JQuey',
            'course_category_id' => 1,
            'price' => '33.000դր'
        ]);

        Course::create([
            'image' => 'c4.jpg',
            'title' => 'PHP & MySQL',
            'course_category_id' => 1,
            'price' => '33.000դր'
        ]);

        Course::create([
            'image' => 'c5.jpg',
            'title' => 'OOP PHP',
            'course_category_id' => 1,
            'price' => '33.000դր'
        ]);
    }
}
