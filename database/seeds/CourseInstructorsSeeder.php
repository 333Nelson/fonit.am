<?php

use App\Models\CourseInstructor;
use Illuminate\Database\Seeder;

class CourseInstructorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // HTML
        CourseInstructor::create([
            'user_id' => 2,
            'course_id' => 1,
        ]);

        CourseInstructor::create([
            'user_id' => 3,
            'course_id' => 1,
        ]);

        // Sass Less
        CourseInstructor::create([
            'user_id' => 3,
            'course_id' => 2,
        ]);

        // Javascript
        CourseInstructor::create([
            'user_id' => 2,
            'course_id' => 3,
        ]);

        CourseInstructor::create([
            'user_id' => 3,
            'course_id' => 3,
        ]);

        // PHP
        CourseInstructor::create([
            'user_id' => 2,
            'course_id' => 4,
        ]);

        CourseInstructor::create([
            'user_id' => 3,
            'course_id' => 4,
        ]);

        // PHP OOP
        CourseInstructor::create([
            'user_id' => 3,
            'course_id' => 5,
        ]);
    }
}
