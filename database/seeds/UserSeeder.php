<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role' => 'admin',
            'name' => 'DryCode',
            'phone' => '093335586',
            'email' => 'drycode2020@gmail.com',
            'password' => Hash::make('!drycode')
        ]);

        User::create([
            'role' => 'instructor',
            'name' => 'Nelson Khachatryan',
            'phone' => '093335586',
            'email' => 'xachatryan.nelsonich@gmail.com',
            'password' => Hash::make('00000000')
        ]);

        User::create([
            'role' => 'instructor',
            'name' => 'Vahe Grigoryan',
            'phone' => '093773107',
            'email' => 'vahegrigoryan179@gmail.com',
            'password' => Hash::make('00000000')
        ]);

        User::create([
            'role' => 'student',
            'name' => 'Student',
            'phone' => '000000000',
            'email' => 'student@gmail.com',
            'password' => Hash::make('00000000')
        ]);
    }
}
