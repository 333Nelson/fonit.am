<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(SiteInformationSeeder::class);
         $this->call(CourseCategoriesSeeder::class);
         $this->call(CoursesSeeder::class);
         $this->call(CourseFeaturesSeeder::class);
         $this->call(CourseInstructorsSeeder::class);
         $this->call(CurriculumSeeder::class);
    }
}
